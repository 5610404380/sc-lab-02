package BControl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import BGUI.BankGUI;
import BModel.BankModel;



public class BankControl {
	  
	private static BankGUI gui = new BankGUI();
	private static BankModel model;
	
public BankControl() {
		
		model = new BankModel(gui.getinit());
		gui.InvestmentFrame(model.getBalance());
		gui.createTextField();
		listener = new AddInterestListener();
		gui.createButton(listener);
		gui.createPanel();
	}
	
	
	class AddInterestListener implements ActionListener {
    	
		public void actionPerformed(ActionEvent event) {
    		double rate = Double.parseDouble(gui.getTextRate());
            double interest = model.getBalance() * rate / 100;
            model.deposit(interest);
            gui.getResultLabel("balance : " + model.getBalance());
            
        }            
    }
	ActionListener listener;
	
	public static void main(String[] args) {
		new BankControl();
		
	}	
	

	
}


	
	
