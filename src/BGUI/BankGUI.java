package BGUI;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;





public class BankGUI extends JFrame {
		private static final double INITIAL_BALANCE = 1000;
		private static final int FRAME_WIDTH = 450;
		private static final int FRAME_HEIGHT = 100;
		
		private static final double DEFAULT_RATE = 5;

		private JLabel rateLabel;
		private JTextField rateField;
		private JButton button;
		private JLabel resultLabel;
		private JPanel panel;
		
		public void InvestmentFrame(double res) {  
		    
			resultLabel = new JLabel("balance : " + res);
		}
		
		public void createTextField() {
			rateLabel = new JLabel("Interest Rate: ");
		    final int FIELD_WIDTH = 10;
		    rateField = new JTextField(FIELD_WIDTH);
		    rateField.setText("" + DEFAULT_RATE);
		}
			
		
		public void createButton(ActionListener listener) {
		    button = new JButton("Add Interest");
		    button.addActionListener(listener);
		}

		public void createPanel() {
		    panel = new JPanel();
		    panel.add(rateLabel);
		    panel.add(rateField);
		    panel.add(button);
		    panel.add(resultLabel);      
		    add(panel);
		    
		    setVisible(true);
		    setSize(FRAME_WIDTH, FRAME_HEIGHT);
		}

		public String getTextRate() {
			return rateField.getText();
		} 
		public void getResultLabel(String result) {
			resultLabel.setText(result);
		}
		public double getinit() {
			return INITIAL_BALANCE;
		} 
		   
	}

